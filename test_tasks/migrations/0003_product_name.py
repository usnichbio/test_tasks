# Generated by Django 3.1.7 on 2021-05-05 12:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('test_tasks', '0002_auto_20210505_1530'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='name',
            field=models.CharField(default=None, max_length=100),
        ),
    ]
