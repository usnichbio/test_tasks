from django.db import models


class Application(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    contract = models.OneToOneField('Contract', on_delete=models.CASCADE,
                                    blank=True, null=True, related_name='applications')


class Contract(models.Model):
    created = models.DateTimeField(auto_now_add=True)


class Firm(models.Model):
    name = models.CharField(max_length=100, default=None)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, default=None)
    firm = models.ForeignKey(Firm, related_name='products',
                             on_delete=models.CASCADE, blank=True, null=True)
    application = models.ForeignKey(Application, related_name='application_product',
                                    on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name





