from django.contrib import admin
from .models import Application, Contract, Firm, Product


class ApplicationAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'contract')
    search_fields = ('id', 'created', 'contract')


class ContractAdmin(admin.ModelAdmin):
    list_display = ('id', 'created')
    search_fields = ('id', 'created')


class FirmAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'name')
    search_fields = ('id', 'created', 'name')


class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'name', 'firm', 'application')
    search_fields = ('id', 'created', 'name', 'firm', 'application')


admin.site.register(Application, ApplicationAdmin)
admin.site.register(Contract, ContractAdmin)
admin.site.register(Firm, FirmAdmin)
admin.site.register(Product, ProductAdmin)
