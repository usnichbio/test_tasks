from django.apps import AppConfig


class TeskTasksConfig(AppConfig):
    name = 'test_tasks'
