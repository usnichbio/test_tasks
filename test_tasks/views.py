from django.shortcuts import render, redirect
from .models import Application, Contract, Firm, Product


def firm(request):
    print(request.GET)
    f_id = request.POST.get('id')
    firms_id = Application.objects.get(contract=f_id).application_product.values_list('firm', flat=True)
    context = {'firms_id': firms_id,
               'f_id': f_id}
    return render(request, 'product.html', context)

